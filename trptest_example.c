// (Only) this source file is under public domain. Do literally whatever you want with it.
// gcc trptest.c trptest_example.c -o trptest_example

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "trptest.h"

static void prv_test_i_eq_5(const TestGroup *testgrp, const TestCase *test) {
    (void)testgrp;

    unsigned int v;
    tt_will_return(prv_test_i_eq_5, "foob", test->data);
    v = tt_mock("foob", unsigned int);
    if(test->data != 5)
        tt_check_eq_i(v, 5);
    else
        tt_assert_eq_i(v, 5);
}

static void prv_test_i_ne_5(const TestGroup *testgrp, const TestCase *test) {
    (void)testgrp;

    unsigned int v;
    tt_will_return(prv_test_i_ne_5, "foob", test->data);
    v = tt_mock("foob", unsigned int);
    if(test->data == 5)
        tt_check_ne_i(v, 5);
    else
        tt_assert_ne_i(v, 5);
}

static void prv_test_str_eq_foobar(const TestGroup *testgrp, const TestCase *test) {
    (void)testgrp;

    const char *v;
    tt_will_return(prv_test_str_eq_foobar, "foob", test->data);
    v = tt_mock("foob", const char *);
    tt_assert_eq_str(v, "foobar");
}

static void prv_test_str_ne_foobar(const TestGroup *testgrp, const TestCase *test) {
    (void)testgrp;

    const char *v;
    tt_will_return(prv_test_str_ne_foobar, "foob", test->data);
    v = tt_mock("foob", const char *);
    tt_assert_ne_str(v, "foobar");
}

static void prv_test_ptr_eq_gtest(const TestGroup *testgrp, const TestCase *test) {
    (void)testgrp;

    void *v;
    tt_will_return(prv_test_ptr_eq_gtest, 0, test->data);
    v = tt_mock(0, void*);
    tt_assert_eq_ptr(v, &g_test);
}

static void prv_test_ptr_ne_gtest(const TestGroup *testgrp, const TestCase *test) {
    (void)testgrp;

    void *v;
    tt_will_return(prv_test_ptr_ne_gtest, 0, test->data);
    v = tt_mock(0, void*);
    tt_assert_ne_ptr(v, &g_test);
}

TestGroup g_test = {
    "trptest_test",
    (TestCase[]){
        { "test_int1", prv_test_i_eq_5, 5, },
        { "test_int2", prv_test_i_ne_5, 4, },
        { "test_int3", prv_test_i_eq_5, 4, },
        { "test_int4", prv_test_i_ne_5, 5, },
        { "test_str1", prv_test_str_eq_foobar, (unsigned long long)"foobar", },
        { "test_str2", prv_test_str_ne_foobar, (unsigned long long)"poobar", },
        { "test_ptr1", prv_test_ptr_eq_gtest, (unsigned long long)&g_test, },
        { "test_ptr2", prv_test_ptr_ne_gtest, (unsigned long long)trptest_printf, },
        TestCaseEnd
    },
    0,//init
    0,//fini
    0,//each_init
    0,//each_fini
};

void trptest_printf(const char *fmt, ...) {
    va_list v;
    va_start(v, fmt);
    vprintf(fmt, v);
    va_end(v);
}

void *trptest_alloc(unsigned long size) {
    return malloc(size);
}

void trptest_free(void *ptr) {
    free(ptr);
}
