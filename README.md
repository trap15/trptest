trptest is a simple unit testing framework. It supports mocking and some other cool stuff.

The design is somewhat inspired by [clar](https://github.com/vmg/clar) and
[cmocka](https://cmocka.org/).

I will put some actual info here at some point, but for now just check out the header and
example test file.

See LICENSE for license information.

Send me an email if this project was useful to you, I'd like to hear about what you're using
it for!
