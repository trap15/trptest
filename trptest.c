/* Copyright (C) 2017 Alex "trap15" Marshall <trap15@raidenii.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

// formatting: 2 space indentation, 100 column line maximum

#include "trptest.h"

#if ALLOW_SJLJ
# include <setjmp.h>
#endif

typedef struct TrptestConfig {
  int upgrade_warnings;
  int stop_on_first_failed_case;
  int stop_on_first_failed_assert;
  int print_succeed_names;
} TrptestConfig;

typedef struct TrptestVector {
  unsigned int size;
  unsigned int count;
  unsigned int unit_size;
  unsigned char *data;
} TrptestVector;

typedef struct TrptestMock {
  const char *func;
  const char *name;
  TrptestVector data; // longlong
} TrptestMock;

typedef struct TrptestState {
  TestCase *current_test;
  unsigned int test_failed;
  unsigned int test_warned;

#if ALLOW_SJLJ
  jmp_buf jmp;
#endif

  TrptestVector mocks; // TrptestMock
} TrptestState;

static TrptestConfig s_config;
static TrptestState s_state;

///////////////////////////////////////////////////////////////////////////////////////////////////
// STDLIB implementation
///////////////////////////////////////////////////////////////////////////////////////////////////

static int prv_tt_strcmp(const char *a, const char *b) {
  while(*a && *a == *b) {
    a++;
    b++;
  }
  return (*a == *b && *a == '\0');
}

static void prv_tt_memmove(void *dst_, void *src_, unsigned long size) {
  unsigned char *dst = (unsigned char*)dst_;
  unsigned char *src = (unsigned char*)src_;
  if((unsigned long long)dst < (unsigned long long)src) {
    while(size--) {
      *(dst++) = *(src++);
    }
  }else{
    dst += size;
    src += size;
    while(size--) {
      *(--dst) = *(--src);
    }
  }
}

static void *prv_tt_realloc(void *ptr, unsigned long newsize, unsigned long oldsize) {
  void *newptr = trptest_alloc(newsize);
  // todo: check allocation
  if(oldsize != 0) {
    unsigned long copysize = newsize > oldsize ? oldsize : newsize;
    prv_tt_memmove(newptr, ptr, copysize);
    trptest_free(ptr);
  }
  return newptr;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// TTVEC implementation
///////////////////////////////////////////////////////////////////////////////////////////////////

static void prv_ttvec_realloc(TrptestVector *vec, unsigned int new_size) {
  vec->data = prv_tt_realloc(vec->data, new_size * vec->unit_size, vec->size * vec->unit_size);
  // todo: check allocation
  vec->size = new_size;
}

static void *prv_ttvec_push(TrptestVector *vec, void *elem) {
  if(vec->count == vec->size) {
    prv_ttvec_realloc(vec, vec->size * 2);
  }
  void *ptr = vec->data + (vec->count * vec->unit_size);
  prv_tt_memmove(ptr, elem, vec->unit_size);
  vec->count++;
  return ptr;
}

static void prv_ttvec_pullfront(TrptestVector *vec, void *dest) {
  prv_tt_memmove(dest, vec->data, vec->unit_size);
  vec->count--;
  prv_tt_memmove(vec->data, vec->data + vec->unit_size, vec->count * vec->unit_size);
}

static void *prv_ttvec_get(TrptestVector *vec, unsigned int idx) {
  return vec->data + (idx * vec->unit_size);
}

static void prv_ttvec_init(TrptestVector *vec, unsigned int initial_size, unsigned int unit_size) {
  vec->size = 0;
  vec->count = 0;
  vec->unit_size = unit_size;
  vec->data = 0;
  prv_ttvec_realloc(vec, initial_size);
}

static void prv_ttvec_fini(TrptestVector *vec) {
  trptest_free(vec->data);
  vec->data = 0;
  vec->count = vec->size = 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// trptest implementation
///////////////////////////////////////////////////////////////////////////////////////////////////

#if ALLOW_SJLJ
# define SJLJ_WRAP() if(!setjmp(s_state.jmp))
#else
# define SJLJ_WRAP() if(1)
#endif

static void prv_tt_fail_out(void) {
#if ALLOW_SJLJ
  longjmp(s_state.jmp, 1);
#endif
}

static void prv_tt_error(void) {
  s_state.current_test->fails++;
}

static void prv_tt_warn(void) {
  s_state.current_test->warns++;
}

static void prv_tt_pass(void) {
  s_state.current_test->passes++;
}

static const char *s_checkstr[2][2] = {
  { "ERROR: tt_assert", "ERROR: tt_check", },
  { "WARNING: tt_assert", "WARNING: tt_check", },
};

#define ASSERTIMPL_TOP(passcond) \
  if(passcond) { \
    prv_tt_pass(); \
    return; \
  } \
  int checktype = check; \
  if(check && s_config.upgrade_warnings) { \
    trptest_printf("WARNING upgraded to ERROR\n"); \
    check = 0; \
  }

#define ASSERTIMPL_BOTTOM() \
  if(check) { \
    prv_tt_warn(); \
  }else{ \
    prv_tt_error(); \
  } \
  if(s_config.stop_on_first_failed_assert && !check) { \
    prv_tt_fail_out(); \
  }

void tt_assert_(int passed, const char *expr,
                const char *file, const char *func, int line, int check) {
  ASSERTIMPL_TOP(passed);
  trptest_printf("%s(%s) failed.\n"
                 "  from: %s:%d in %s\n"
                 "  !(%s)\n",
                 s_checkstr[check][checktype], expr, file, line, func, expr);
  ASSERTIMPL_BOTTOM();
}
void tt_assert_eq_i_(long long a, long long b, const char *astr, const char *bstr,
                     const char *file, const char *func, int line, int check) {
  ASSERTIMPL_TOP(a == b);
  trptest_printf("%s_eq_i(%s, %s) failed.\n"
                 "  from: %s:%d in %s\n"
                 "  %lld != %lld\n",
                 s_checkstr[check][checktype], astr, bstr, file, line, func, a, b);
  ASSERTIMPL_BOTTOM();
}
void tt_assert_eq_ptr_(const void *a, const void *b, const char *astr, const char *bstr,
                       const char *file, const char *func, int line, int check) {
  ASSERTIMPL_TOP(a == b);
  trptest_printf("%s_eq_ptr(%s, %s) failed.\n"
                 "  from: %s:%d in %s\n"
                 "  %p != %p\n",
                 s_checkstr[check][checktype], astr, bstr, file, line, func, a, b);
  ASSERTIMPL_BOTTOM();
}
void tt_assert_eq_str_(const char *a, const char *b, const char *astr, const char *bstr,
                       const char *file, const char *func, int line, int check) {
  ASSERTIMPL_TOP(prv_tt_strcmp(a, b));
  trptest_printf("%s_eq_str(%s, %s) failed.\n"
                 "  from: %s:%d in %s\n"
                 "  '%s' != '%s'\n",
                 s_checkstr[check][checktype], astr, bstr, file, line, func, a, b);
  ASSERTIMPL_BOTTOM();
}
void tt_assert_ne_i_(long long a, long long b, const char *astr, const char *bstr,
                     const char *file, const char *func, int line, int check) {
  ASSERTIMPL_TOP(a != b);
  trptest_printf("%s_ne_i(%s, %s) failed.\n"
                 "  from: %s:%d in %s\n"
                 "  %lld == %lld\n",
                 s_checkstr[check][checktype], astr, bstr, file, line, func, a, b);
  ASSERTIMPL_BOTTOM();
}
void tt_assert_ne_ptr_(const void *a, const void *b, const char *astr, const char *bstr,
                       const char *file, const char *func, int line, int check) {
  ASSERTIMPL_TOP(a != b);
  trptest_printf("%s_ne_ptr(%s, %s) failed.\n"
                 "  from: %s:%d in %s\n"
                 "  %p == %p\n",
                 s_checkstr[check][checktype], astr, bstr, file, line, func, a, b);
  ASSERTIMPL_BOTTOM();
}
void tt_assert_ne_str_(const char *a, const char *b, const char *astr, const char *bstr,
                       const char *file, const char *func, int line, int check) {
  ASSERTIMPL_TOP(!prv_tt_strcmp(a, b));
  trptest_printf("%s_ne_str(%s, %s) failed.\n"
                 "  from: %s:%d in %s\n"
                 "  '%s' == '%s'\n",
                 s_checkstr[check][checktype], astr, bstr, file, line, func, a, b);
  ASSERTIMPL_BOTTOM();
}

long long tt_mock_(const char *func, const char *mockname) {
  if(mockname == 0) mockname = "";

  TrptestMock *mock = 0;
  for(unsigned long i = 0; i < s_state.mocks.count; i++) {
    mock = prv_ttvec_get(&s_state.mocks, i);

    if(prv_tt_strcmp(func, mock->func) &&
       prv_tt_strcmp(mockname, mock->name)) {
      break;
    }
    mock = 0;
  }
  if(mock == 0) {
    trptest_printf("ERROR: Attempt to consume unavailable mock!\n"
                   "  %s:%s does not exist.\n",
                   func, mockname);
    prv_tt_error();
    prv_tt_fail_out();
    return 0;
  }

  if(mock->data.count == 0) {
    trptest_printf("ERROR: Attempt to consume unavailable mock!\n"
                   "  %s:%s has no remaining data.\n",
                   func, mockname);
    prv_tt_error();
    prv_tt_fail_out();
    return 0;
  }

  long long data;
  prv_ttvec_pullfront(&mock->data, &data);
  return data;
}

void tt_will_return_(const char *func, const char *mockname, long long value) {
  if(mockname == 0) mockname = "";

  TrptestMock *mock = 0;
  for(unsigned long i = 0; i < s_state.mocks.count; i++) {
    mock = prv_ttvec_get(&s_state.mocks, i);
    if(prv_tt_strcmp(func, mock->func) &&
       prv_tt_strcmp(mockname, mock->name)) {
      break;
    }
    mock = 0;
  }
  if(mock == 0) {
    TrptestMock newmock;
    newmock.func = func;
    newmock.name = mockname;
    prv_ttvec_init(&newmock.data, 4, sizeof(long long));
    mock = prv_ttvec_push(&s_state.mocks, &newmock);
  }

  prv_ttvec_push(&mock->data, &value);
}

typedef struct CmdArg {
  const char *longname;
  const char *shortname;
  int params;
  const char *help;
} CmdArg;

enum {
  CmdArg_Help,
  CmdArg_Exclude,
  CmdArg_Include,
  CmdArg_ListSuccess,
  CmdArg_UpgradeWarning,
  CmdArg_RunAllTests,
  CmdArg_RunEntireTest,
  CmdArgCount,
};

static const CmdArg s_args[CmdArgCount] = {
  [CmdArg_Help] = {
    "--help", "-h", 0,
    "Print help message."
  },
  [CmdArg_Exclude] = {
    "--exclude", "-e", 1,
    "Exclude one test from running."
  },
  [CmdArg_Include] = {
    "--include", "-i", 1,
    "Include one test for running."
  },
  [CmdArg_ListSuccess] = {
    "--show-passing", "-s", 0,
    "Shows list of passing tests."
  },
  [CmdArg_UpgradeWarning] = {
    "--fail-warnings", "-w", 0,
    "Count warnings as failures."
  },
  [CmdArg_RunAllTests] = {
    "--force", "-f", 0,
    "Run all tests, even if one fails."
  },
  [CmdArg_RunEntireTest] = {
    "--assert-continues", "-c", 0,
    "Does not end a test if an assert fails."
  },
};

static int prv_handle_arg(const char *app, const char *arg, int argc, const char *argv[]) {
  int argidx = 0;
  for(argidx = 0; argidx < CmdArgCount; argidx++) {
    if(s_args[argidx].params > argc) {
      continue;
    }
    if(prv_tt_strcmp(arg, s_args[argidx].shortname)) {
      break;
    }
    if(prv_tt_strcmp(arg, s_args[argidx].longname)) {
      break;
    }
  }

  switch(argidx) {
    default:
    case CmdArgCount:
      trptest_printf("Unknown argument '%s'.\n", arg);
      argidx = CmdArg_Help;
      // fall-thru
    case CmdArg_Help:
      trptest_printf(
          "trptest (C)2017 Alex \"trap15\" Marshall <trap15@raidenii.net>\n"
          "Usage:\n    %s [options]\n",
          app);
      for(int i = 0; i < CmdArgCount; i++) {
        trptest_printf("%20s, %-2s %-8s %s\n", s_args[i].longname, s_args[i].shortname,
                       s_args[i].params ? "[args]" : "",
                       s_args[i].help);
      }
      return -1;
    case CmdArg_Exclude:
      trptest_printf("<todo>\n");
      return -1;
    case CmdArg_Include:
      trptest_printf("<todo>\n");
      return -1;
    case CmdArg_ListSuccess:
      s_config.print_succeed_names = 1;
      return 0;
    case CmdArg_UpgradeWarning:
      s_config.upgrade_warnings = 1;
      return 0;
    case CmdArg_RunAllTests:
      s_config.stop_on_first_failed_case = 0;
      return 0;
    case CmdArg_RunEntireTest:
      s_config.stop_on_first_failed_assert = 0;
      return 0;
  }
}

enum ReturnCode {
  ReturnCode_Help = -2,
  ReturnCode_Unknown = -1,
  ReturnCode_Success = 0,
  ReturnCode_FailedTest,
  ReturnCode_GroupInitError,
  ReturnCode_GroupFiniError,
  ReturnCode_EachInitError,
  ReturnCode_EachFiniError,
  ReturnCode_TestNameOverlap,
};

int main(int argc, const char *argv[]) {
  TestGroup *testgrp = &g_test;

  s_config.print_succeed_names = 0;
  s_config.upgrade_warnings = 0;
#if ALLOW_SJLJ
  s_config.stop_on_first_failed_assert = 1;
#else
  s_config.stop_on_first_failed_assert = 0;
#endif
  s_config.stop_on_first_failed_case = 1;

  for(int i = 1; i < argc; i++) {
    const char *arg = argv[i];
    if(arg[0] == '-') {
      if(arg[1] == '\0') {
        break;
      }
      int ret = prv_handle_arg(argv[0], arg, argc - i - 1, argv+i+1);
      if(ret < 0) {
        return ReturnCode_Help;
      }
      i += ret;
    }
  }

  if(testgrp->init) {
    SJLJ_WRAP() {
      testgrp->init(testgrp);
    }else{
      trptest_printf("Error in test group init. Aborting.\n");
      return ReturnCode_GroupInitError;
    }
  }

  unsigned int tests_count = 0;
  for(TestCase *test = testgrp->tests; test->name; test++) {
    for(TestCase *test2 = test + 1; test2->name; test2++) {
      if(prv_tt_strcmp(test->name, test2->name)) {
        trptest_printf("Two tests with same name '%s' exist. Aborting.\n", test->name);
        return ReturnCode_TestNameOverlap;
      }
    }

    tests_count++;
    test->ran = 0;
    test->fails = 0;
    test->warns = 0;
    test->passes = 0;
  }

  unsigned int tests_failed = 0;
  unsigned int tests_warned = 0;
  unsigned int tests_passed = 0;
  unsigned int tests_not_run = tests_count;
  for(TestCase *test = testgrp->tests; test->name; test++) {
    prv_ttvec_init(&s_state.mocks, 4, sizeof(TrptestMock));
    s_state.current_test = test;

    if(testgrp->each_init) {
      SJLJ_WRAP() {
        testgrp->each_init(testgrp, test);
      }else{
        trptest_printf("Error in test group each_init. Aborting.\n");
        return ReturnCode_EachInitError;
      }
    }

    trptest_printf("Running %s:%s\n", testgrp->name, test->name);

    tests_not_run--;
    test->ran = 1;

    SJLJ_WRAP() {
      test->impl(testgrp, test);
    }

    if(s_state.mocks.count) {
      TrptestMock *mock;
      for(unsigned long i = 0; i < s_state.mocks.count; i++) {
        mock = prv_ttvec_get(&s_state.mocks, i);

        if(mock->data.count) {
          trptest_printf("ERROR: Not all mock values for %s:%s consumed!\n"
                         "  %d remain.\n",
                         mock->func, mock->name, mock->data.count);
          test->fails++;
        }
      }
    }

    if(test->fails) {
      trptest_printf("-> !FAILED! %d times\n", test->fails);
      tests_failed++;
    }
    if(test->warns) {
      trptest_printf("-> !WARNED! %d times\n", test->warns);
      tests_warned++;
    }
    if(!test->fails && !test->warns) {
      trptest_printf("-> PASSED\n");
      tests_passed++;
    }

    if(s_config.stop_on_first_failed_case &&
       (test->fails ||
        (s_config.upgrade_warnings && test->warns))) {
      break;
    }

    if(testgrp->each_fini) {
      SJLJ_WRAP() {
        testgrp->each_fini(testgrp, test);
      }else{
        trptest_printf("Error in test group each_fini. Aborting.\n");
        return ReturnCode_EachFiniError;
      }
    }

    if(s_state.mocks.count) {
      TrptestMock *mock;
      for(unsigned long i = 0; i < s_state.mocks.count; i++) {
        mock = prv_ttvec_get(&s_state.mocks, i);
        prv_ttvec_fini(&mock->data);
      }
    }
    prv_ttvec_fini(&s_state.mocks);
  }

  trptest_printf("Run summary:\n");
  trptest_printf("  %d tests total (%d not run)\n", tests_count, tests_not_run);
  trptest_printf("  %d passed\n", tests_passed);
  if(s_config.print_succeed_names) {
    for(TestCase *test = testgrp->tests; test->name; test++) {
      if(test->ran && !test->fails && !test->warns) {
        trptest_printf("    - %s:%s\n", testgrp->name, test->name);
      }
    }
  }
  if(tests_warned) {
    trptest_printf("  %d WARNED\n", tests_warned);
    for(TestCase *test = testgrp->tests; test->name; test++) {
      if(test->ran && test->warns) {
        trptest_printf("    - %s:%s\n", testgrp->name, test->name);
      }
    }
  }
  if(tests_failed) {
    trptest_printf("  %d FAILED\n", tests_failed);
    for(TestCase *test = testgrp->tests; test->name; test++) {
      if(test->ran && test->fails) {
        trptest_printf("    - %s:%s\n", testgrp->name, test->name);
      }
    }
  }

  if(testgrp->fini) {
    SJLJ_WRAP() {
      testgrp->fini(testgrp);
    }else{
      trptest_printf("Error in test group fini. Aborting.\n");
      return ReturnCode_GroupFiniError;
    }
  }

  if(tests_failed)
    return ReturnCode_FailedTest;
  if(tests_warned)
    return ReturnCode_Success;
  return ReturnCode_Success;
}
