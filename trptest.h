/* Copyright (C) 2017 Alex "trap15" Marshall <trap15@raidenii.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

// formatting: 2 space indentation, 100 column line maximum

#ifndef TRPTEST_H_
#define TRPTEST_H_

typedef struct TestCase TestCase;
typedef struct TestGroup TestGroup;

/*! \brief The structure defining each individual test.
 */
struct TestCase {
  //! Name of the test. Must be unique.
  const char *name;
  /*! \brief Function to run for the test.
   *
   * \param testgrp The currently running TestGroup.
   * \param test The currently running TestCase.
   */
  void (*impl)(const TestGroup *testgrp, const TestCase *test);

  //! Optional data.
  unsigned long long data;

  // Internal state variables.
  unsigned int ran, passes, fails, warns;
};

//! Sentinel for TestGroup.tests
#define TestCaseEnd {.name=0}

/*! \brief The core structure for your test.
 */
struct TestGroup {
  //! Name of the test group.
  const char *name;

  /*! List of tests.
   *
   * Must end with TestCaseEnd.
   */
  TestCase *tests;

  /*! \brief Function run before any tests.
   *         NULLable.
   *
   * \param testgrp The currently running TestGroup.
   */
  void (*init)(const TestGroup *testgrp);

  /*! \brief Function run after all tests.
   *         NULLable.
   *
   * \param testgrp The currently running TestGroup.
   */
  void (*fini)(const TestGroup *testgrp);

  /*! \brief Function run before each test.
   *         NULLable.
   *
   * \param testgrp The currently running TestGroup.
   * \param test The TestCase that is about to run.
   */
  void (*each_init)(const TestGroup *testgrp, const TestCase *test);

  /*! \brief Function run after each test.
   *         NULLable.
   *
   * \param testgrp The currently running TestGroup.
   * \param test The TestCase that just finished running.
   */
  void (*each_fini)(const TestGroup *testgrp, const TestCase *test);
};

//! An extern definition of the only TestGroup that you should have.
extern TestGroup g_test;

//! Use this attribute to allow overriding an original implementation.
#define MOCKABLE __attribute__((__weak__))

/*! \brief Assert a condition.
 *
 * If the condition is false, the test is considered failed.
 *
 * \param passed Assert unless this evaluates to true.
 *
 * \sa tt_check()
 */
#define tt_assert(passed) \
  tt_assert_(passed, #passed, __FILE__, __FUNCTION__, __LINE__, 0)

/*! \brief Assert that two integers are equal.
 *
 * If they are not equal, the test is considered failed.
 *
 * \param a Integer to compare to b.
 * \param b Integer to compare to a.
 *
 * \sa tt_assert_ne_i(), tt_check_eq_i(), tt_check_ne_i()
 */
#define tt_assert_eq_i(a, b) \
  tt_assert_eq_i_((long long)(a), (long long)(b), #a, #b, __FILE__, __FUNCTION__, __LINE__, 0)

/*! \brief Assert that two pointers are equal.
 *
 * If they are not equal, the test is considered failed.
 *
 * \param a Pointer to compare to b.
 * \param b Pointer to compare to a.
 *
 * \sa tt_assert_ne_ptr(), tt_check_eq_ptr(), tt_check_ne_ptr()
 */
#define tt_assert_eq_ptr(a, b) \
  tt_assert_eq_ptr_((const void*)(a), (const void*)(b), #a, #b, __FILE__, __FUNCTION__, __LINE__, 0)

/*! \brief Assert that two strings have the same content.
 *
 * If they are not equal, the test is considered failed.
 *
 * \param a String to compare to b.
 * \param b String to compare to a.
 *
 * \sa tt_assert_ne_str(), tt_check_eq_str(), tt_check_ne_str()
 */
#define tt_assert_eq_str(a, b) \
  tt_assert_eq_str_(a, b, #a, #b, __FILE__, __FUNCTION__, __LINE__, 0)

/*! \brief Assert that two integers are not equal.
 *
 * If they are equal, the test is considered failed.
 *
 * \param a Integer to compare to b.
 * \param b Integer to compare to a.
 *
 * \sa tt_assert_eq_i(), tt_check_eq_i(), tt_check_ne_i()
 */
#define tt_assert_ne_i(a, b) \
  tt_assert_ne_i_((long long)(a), (long long)(b), #a, #b, __FILE__, __FUNCTION__, __LINE__, 0)

/*! \brief Assert that two pointers are not equal.
 *
 * If they are equal, the test is considered failed.
 *
 * \param a Pointer to compare to b.
 * \param b Pointer to compare to a.
 *
 * \sa tt_assert_eq_ptr(), tt_check_eq_ptr(), tt_check_ne_ptr()
 */
#define tt_assert_ne_ptr(a, b) \
  tt_assert_ne_ptr_((const void*)(a), (const void*)(b), #a, #b, __FILE__, __FUNCTION__, __LINE__, 0)

/*! \brief Assert that two strings have different content.
 *
 * If they are equal, the test is considered failed.
 *
 * \param a String to compare to b.
 * \param b String to compare to a.
 *
 * \sa tt_assert_eq_str(), tt_check_eq_str(), tt_check_ne_str()
 */
#define tt_assert_ne_str(a, b) \
  tt_assert_ne_str_(a, b, #a, #b, __FILE__, __FUNCTION__, __LINE__, 0)

/*! \brief Check a condition.
 *
 * If the condition is false, a warning is issued, but the test is not considered failed.
 *
 * \param passed Warn unless this evaluates to true.
 *
 * \sa tt_assert()
 */
#define tt_check(passed) \
  tt_assert_(passed, #passed, __FILE__, __FUNCTION__, __LINE__, 1)

/*! \brief Check that two integers are equal.
 *
 * If they are not equal, a warning is issued, but the test is not considered failed.
 *
 * \param a Integer to compare to b.
 * \param b Integer to compare to a.
 *
 * \sa tt_check_ne_i(), tt_assert_eq_i(), tt_assert_ne_i()
 */
#define tt_check_eq_i(a, b) \
  tt_assert_eq_i_((long long)(a), (long long)(b), #a, #b, __FILE__, __FUNCTION__, __LINE__, 1)

/*! \brief Check that two pointers are equal.
 *
 * If they are not equal, a warning is issued, but the test is not considered failed.
 *
 * \param a Pointer to compare to b.
 * \param b Pointer to compare to a.
 *
 * \sa tt_check_ne_ptr(), tt_assert_eq_ptr(), tt_assert_ne_ptr()
 */
#define tt_check_eq_ptr(a, b) \
  tt_assert_eq_ptr_((const void*)(a), (const void*)(b), #a, #b, __FILE__, __FUNCTION__, __LINE__, 1)

/*! \brief Check that two strings have the same content.
 *
 * If they are not equal, a warning is issued, but the test is not considered failed.
 *
 * \param a String to compare to b.
 * \param b String to compare to a.
 *
 * \sa tt_check_ne_str(), tt_assert_eq_str(), tt_assert_ne_str()
 */
#define tt_check_eq_str(a, b) \
  tt_assert_eq_str_(a, b, #a, #b, __FILE__, __FUNCTION__, __LINE__, 1)

/*! \brief Check that two integers are not equal.
 *
 * If they are equal, a warning is issued, but the test is not considered failed.
 *
 * \param a Integer to compare to b.
 * \param b Integer to compare to a.
 *
 * \sa tt_check_eq_i(), tt_assert_eq_i(), tt_assert_ne_i()
 */
#define tt_check_ne_i(a, b) \
  tt_assert_ne_i_((long long)(a), (long long)(b), #a, #b, __FILE__, __FUNCTION__, __LINE__, 1)

/*! \brief Check that two pointers are not equal.
 *
 * If they are equal, a warning is issued, but the test is not considered failed.
 *
 * \param a Pointer to compare to b.
 * \param b Pointer to compare to a.
 *
 * \sa tt_check_eq_ptr(), tt_assert_eq_ptr(), tt_assert_ne_ptr()
 */
#define tt_check_ne_ptr(a, b) \
  tt_assert_ne_ptr_((const void*)(a), (const void*)(b), #a, #b, __FILE__, __FUNCTION__, __LINE__, 1)

/*! \brief Check that two strings have different content.
 *
 * If they are equal, a warning is issued, but the test is not considered failed.
 *
 * \param a String to compare to b.
 * \param b String to compare to a.
 *
 * \sa tt_check_eq_str(), tt_assert_eq_str(), tt_assert_ne_str()
 */
#define tt_check_ne_str(a, b) \
  tt_assert_ne_str_(a, b, #a, #b, __FILE__, __FUNCTION__, __LINE__, 1)

/*! \brief Retrieve a test value.
 *
 * Pulls a test value that was specified by tt_will_return for this function.
 *
 * \param name Test value FIFO name, or NULL.
 * \param type Type of the value to be returned.
 *
 * \sa tt_will_return()
 */
#define tt_mock(name, type) \
  ((type)tt_mock_(__FUNCTION__, name))

/*! \brief Add a test value.
 *
 * Adds a test value for a function.
 *
 * \param function Function that this value will be added for.
 * \param name Test value FIFO name, or NULL.
 * \param value Value to add to the test FIFO.
 *
 * \sa tt_mock()
 */
#define tt_will_return(function, name, value) \
  tt_will_return_(#function, name, (long long)(value))

///////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation details from here onwards!
///////////////////////////////////////////////////////////////////////////////////////////////////

// Set this to 0 if your target lacks setjmp/longjmp implementation and/or setjmp.h
// With this disabled, tests cannot exit early, so it's essentially acting as if --assert-continues
// flag was passed (this option is forced on with this setting disabled).
#define ALLOW_SJLJ 1

// These need to be implemented by you, they're wrappers against libc
// functions.
void trptest_printf(const char *fmt, ...); // Alias me to printf
void *trptest_alloc(unsigned long size); // Alias me to malloc
void trptest_free(void *ptr); // Alias me to free

// Internal assert implementations, don't use these
void tt_assert_(int passed, const char *expr,
                const char *file, const char *func, int line, int check);
void tt_assert_eq_i_(long long a, long long b, const char *astr, const char *bstr,
                     const char *file, const char *func, int line, int check);
void tt_assert_eq_ptr_(const void *a, const void *b, const char *astr, const char *bstr,
                       const char *file, const char *func, int line, int check);
void tt_assert_eq_str_(const char *a, const char *b, const char *astr, const char *bstr,
                       const char *file, const char *func, int line, int check);
void tt_assert_ne_i_(long long a, long long b, const char *astr, const char *bstr,
                     const char *file, const char *func, int line, int check);
void tt_assert_ne_ptr_(const void *a, const void *b, const char *astr, const char *bstr,
                       const char *file, const char *func, int line, int check);
void tt_assert_ne_str_(const char *a, const char *b, const char *astr, const char *bstr,
                       const char *file, const char *func, int line, int check);
long long tt_mock_(const char *func, const char *mockname);
void tt_will_return_(const char *func, const char *mockname, long long value);

#endif
